package threads.server.core.files;

import androidx.room.RoomDatabase;

@androidx.room.Database(entities = {Proxy.class}, version = 1, exportSchema = false)
public abstract class FilesDatabase extends RoomDatabase {

    public abstract ProxyDao proxyDao();

}
