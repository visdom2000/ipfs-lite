package threads.server.services;

import android.content.Context;
import android.net.nsd.NsdServiceInfo;

import androidx.annotation.NonNull;

import net.luminis.quic.QuicConnection;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.cid.Protocol;
import threads.lite.host.Session;

public class LocalConnectService {

    private static final String TAG = LocalConnectService.class.getSimpleName();

    public static void connect(@NonNull Session session, @NonNull Context context,
                               @NonNull NsdServiceInfo serviceInfo) {
        IPFS ipfs = IPFS.getInstance(context);
        AtomicBoolean regularNode = new AtomicBoolean(false);
        try {
            PeerId.decodeName(serviceInfo.getServiceName());
        } catch (Throwable throwable) {
            regularNode.set(true);
        }

        Executors.newSingleThreadExecutor().execute( () -> {
            try {
                if (!regularNode.get()) {
                    InetAddress inetAddress = serviceInfo.getHost();

                    String pre = "/ip4";
                    if (inetAddress instanceof Inet6Address) {
                        pre = "/ip6";
                    }

                    String multiAddress = pre + inetAddress + "/udp/" + serviceInfo.getPort() + "/quic";
                    Multiaddr multiaddr = Multiaddr.create(multiAddress);

                    if (!session.swarmHas(multiaddr)) {
                        QuicConnection conn = ipfs.dial(session, multiaddr, IPFS.CONNECT_TIMEOUT,
                                IPFS.GRACE_PERIOD, IPFS.MAX_STREAMS, IPFS.MESSAGE_SIZE_MAX);
                        session.swarmEnhance(conn);

                        LogUtils.error(TAG, "Success adding to swarm " + multiaddr);
                    }
                } else {
                    Map<String, byte[]> maps = serviceInfo.getAttributes();
                    byte[] value = maps.get("dnsaddr");
                    Multiaddr multiaddr = Multiaddr.create(new String(value));

                    PeerId peer = PeerId.decodeName(Objects.requireNonNull(
                            multiaddr.getStringComponent(Protocol.P2P)));

                    QuicConnection conn = ipfs.connectPeer(session, peer, IPFS.CONNECT_TIMEOUT,
                                    IPFS.MAX_STREAMS, IPFS.MESSAGE_SIZE_MAX, true)
                            .get(30, TimeUnit.SECONDS);
                    session.swarmEnhance(conn);

                    LogUtils.error(TAG, "Success adding to swarm " + conn.getRemoteAddress());

                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });
    }

}

