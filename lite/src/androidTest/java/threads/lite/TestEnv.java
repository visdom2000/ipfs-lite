package threads.lite;

import android.content.Context;

import androidx.annotation.NonNull;

class TestEnv {


    public static IPFS getTestInstance(@NonNull Context context) {

        IPFS ipfs = IPFS.getInstance(context);

        ipfs.clearDatabase();
        ipfs.updateNetwork();

        return ipfs;
    }


}
