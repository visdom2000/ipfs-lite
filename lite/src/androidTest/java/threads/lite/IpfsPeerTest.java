package threads.lite;


import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import junit.framework.TestCase;

import net.luminis.quic.QuicConnection;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.host.Session;
import threads.lite.relay.Reservation;

@RunWith(AndroidJUnit4.class)
public class IpfsPeerTest {
    private static final String TAG = IpfsPeerTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    //@Test
    public void find_peers() throws ExecutionException, InterruptedException, TimeoutException {

        // 12D3KooWN4KT1S6yvcHWxLtSZNfUdtpzweR9eWUku3PHbhGuEXVa
        // 12D3KooWDNoFXDdM6uHGYgg3DATiBEj5UPHS6VtV3x93xNPAVs88
        // 12D3KooWLcGqBfrFRyfgbPPLm9zVb5QAm3DZAoRi22DH21yE4TgN
        // 12D3KooWMEJ2RFuNdLR1u6sQ98STTPCJGrphZcqQFMecXRSzeYLB
        // 12D3KooWS7nJWAsGo63mNVK4bDDauTJderb8MZRXcjkQERQqMvUY
        IPFS ipfs = TestEnv.getTestInstance(context);
        Session session = ipfs.createSession();
        try {
            PeerId peerId = PeerId.fromBase58("12D3KooWN4KT1S6yvcHWxLtSZNfUdtpzweR9eWUku3PHbhGuEXVa");

            CompletableFuture<QuicConnection> future = ipfs.connectPeer(session,
                    peerId, IPFS.GRACE_PERIOD, 0, IPFS.MESSAGE_SIZE_MAX, false);
            QuicConnection conn = future.get(30, TimeUnit.SECONDS);
            assertNotNull(conn);
            conn.close();
        } finally {
            session.clear(true);
        }
    }


    @Test
    public void test_connectAndFindPeer() throws InterruptedException, ExecutionException, TimeoutException {

        IPFS ipfs = TestEnv.getTestInstance(context);

        Session session = ipfs.createSession();
        try {
            Multiaddr multiaddr = Multiaddr.create("/ip4/139.178.68.145/udp/4001/quic");
            assertTrue(multiaddr.isIP4());
            PeerId peerId = ipfs.getPeerId("12D3KooWSzPeHsfxULJwFiLeq6Qsx6TruezAwjZ619qsLhqC7cUR");
            QuicConnection conn = ipfs.connect(session, multiaddr, IPFS.CONNECT_TIMEOUT,
                    IPFS.GRACE_PERIOD, 0).get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
            assertNotNull(conn);
            TestCase.assertTrue(conn.isConnected());

            try {
                ipfs.notify(conn, "hello").get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
                fail();
            } catch (Throwable throwable) {
                LogUtils.info(TAG, throwable.getMessage());
            }

            AtomicBoolean found = new AtomicBoolean(false);
            ipfs.findPeer(session, peerId, multiaddr1 -> {
                LogUtils.debug(TAG, multiaddr1.toString());
                found.set(true);
            }, false, found::get).get(30, TimeUnit.SECONDS);
            assertTrue(found.get());
            conn.close();
            Thread.sleep(1000);
            CompletableFuture<QuicConnection> future = ipfs.connectPeer(ipfs.getSession(),
                    peerId, IPFS.CONNECT_TIMEOUT,
                    0, IPFS.MESSAGE_SIZE_MAX, false);
            conn = future.get(30, TimeUnit.SECONDS);
            assertNotNull(conn);


            conn.close();
        } finally {
            session.clear(true);
        }
    }

    @Test
    public void test_findPeer() throws ExecutionException, InterruptedException, TimeoutException {

        IPFS ipfs = TestEnv.getTestInstance(context);
        Session session = ipfs.createSession();
        try {
            ConcurrentHashMap<PeerId, Reservation> reservations = ipfs.reservations();
            assertFalse(reservations.isEmpty());
            for (Reservation reservation : reservations.values()) {
                PeerId relayId = reservation.getPeerId();
                LogUtils.debug(TAG, relayId.toString());
                QuicConnection conn = reservation.getConnection();
                assertNotNull(conn);
                assertTrue(conn.isConnected());
                AtomicBoolean found = new AtomicBoolean(false);
                ipfs.findPeer(session, relayId, multiaddr -> {
                    LogUtils.debug(TAG, multiaddr.toString());
                    found.set(true);
                }, false, found::get).get(30, TimeUnit.SECONDS);
                // assertTrue(found.get());
            }
        } finally {
            session.clear(true);
        }
    }
}