package threads.lite;


import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import net.luminis.quic.QuicConnection;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.cid.PeerId;
import threads.lite.core.TimeoutCancellable;
import threads.lite.host.Session;
import threads.lite.ipns.Ipns;


@RunWith(AndroidJUnit4.class)
public class IpfsFindPeerTest {
    private static final String TAG = IpfsFindPeerTest.class.getSimpleName();


    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    //@Test
    public void find_pc() throws ExecutionException, InterruptedException, TimeoutException {

        IPFS ipfs = TestEnv.getTestInstance(context);
        Session session = ipfs.createSession();
        try {
            PeerId pc = PeerId.fromBase58("12D3KooWSR9wEwGHjd6Rj3CViDPvHX5FGkXop4y6wNVzRvbZSXoK");
            AtomicBoolean found = new AtomicBoolean(false);
            ipfs.findPeer(session, pc,
                    multiaddr -> {
                        LogUtils.error(TAG, multiaddr.toString());
                        found.set(true);
                    }, true, found::get).get(30, TimeUnit.SECONDS);

            assertTrue(found.get());


            QuicConnection conn = ipfs.connectPeer(session, pc,
                            IPFS.CONNECT_TIMEOUT, IPFS.MAX_STREAMS, IPFS.GRACE_PERIOD, true)
                    .get(30, TimeUnit.SECONDS);
            assertNotNull(conn);


        } finally {
            session.clear(true);
        }
    }

    @Test
    public void find_peer_test0() throws ExecutionException, InterruptedException, TimeoutException {

        IPFS ipfs = TestEnv.getTestInstance(context);
        Session session = ipfs.createSession();
        try {
            PeerId relay = PeerId.fromBase58("QmW9m57aiBDHAkKj9nmFSEn7ZqrcF1fZS4bipsTCHburei");

            AtomicBoolean found = new AtomicBoolean(false);
            ipfs.findPeer(session, relay, multiaddr -> {
                LogUtils.debug(TAG, multiaddr.toString());
                found.set(true);
            }, false, found::get).get(10, TimeUnit.SECONDS);

            assertTrue(found.get());
        } finally {
            session.clear(true);
        }

    }

    @Test
    public void find_peer_corbett() throws IOException {
        IPFS ipfs = TestEnv.getTestInstance(context);

        //CorbettReport ipns://k2k4r8jllj4k33jxoa4vaeleqkrwu8b7tqz7tgczhptbfkhqr2i280fm
        Session session = ipfs.createSession();
        try {
            String key = "k2k4r8jllj4k33jxoa4vaeleqkrwu8b7tqz7tgczhptbfkhqr2i280fm";
            Ipns.Entry res = ipfs.resolveName(session, PeerId.decodeName(key),
                    0, new TimeoutCancellable(30));
            assertNotNull(res);
            LogUtils.debug(TAG, res.getPeerId().toBase58());
            LogUtils.debug(TAG, res.getKeyType().toString());
            LogUtils.debug(TAG, res.getValue());
            LogUtils.debug(TAG, res.toString());
            assertTrue(res.getHash().isSupported());
        } finally {
            session.clear(true);
        }
    }

    @Test
    public void find_peer_freedom() throws IOException {
        IPFS ipfs = TestEnv.getTestInstance(context);

        //FreedomsPhoenix.com ipns://k2k4r8magsykrprepvtuvd1h8wonxy7rbdkxd09aalsvclqh7wpb28m1
        Session session = ipfs.createSession();
        try {
            String key = "k2k4r8magsykrprepvtuvd1h8wonxy7rbdkxd09aalsvclqh7wpb28m1";
            Ipns.Entry res = ipfs.resolveName(session, PeerId.decodeName(key),
                    0, new TimeoutCancellable(30));
            assertNotNull(res);
            LogUtils.debug(TAG, res.getPeerId().toBase58());
            LogUtils.debug(TAG, res.getKeyType().toString());
            LogUtils.debug(TAG, res.getValue());
            LogUtils.debug(TAG, res.toString());
            assertTrue(res.getHash().isSupported());
        } finally {
            session.clear(true);
        }
    }

    @Test
    public void find_peer_pirates() throws IOException {
        IPFS ipfs = TestEnv.getTestInstance(context);

        //PiratesWithoutBorders.com ipns://k2k4r8l8zgv45qm2sjt7p16l7pvy69l4jr1o50cld4s98wbnanl0zn6t
        Session session = ipfs.createSession();
        try {
            String key = "k2k4r8l8zgv45qm2sjt7p16l7pvy69l4jr1o50cld4s98wbnanl0zn6t";
            Ipns.Entry res = ipfs.resolveName(session, PeerId.decodeName(key),
                    0, new TimeoutCancellable(30));
            assertNotNull(res);
            LogUtils.debug(TAG, res.getPeerId().toBase58());
            LogUtils.debug(TAG, res.getKeyType().toString());
            LogUtils.debug(TAG, res.getValue());
            LogUtils.debug(TAG, res.toString());
            assertTrue(res.getHash().isSupported());
        } finally {
            session.clear(true);
        }
    }
}
