package threads.lite;


import static org.junit.Assert.assertNotNull;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import net.luminis.quic.QuicConnection;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import threads.lite.cid.Multiaddr;
import threads.lite.host.Session;

@RunWith(AndroidJUnit4.class)
public class IpfsDnsAddressTest {

    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_dnsAddress() throws InterruptedException, ExecutionException, TimeoutException {
        IPFS ipfs = TestEnv.getTestInstance(context);
        Session session = ipfs.createSession();
        try {
            Multiaddr peer = Multiaddr.create(
                    "/dnsaddr/bootstrap.libp2p.io/p2p/QmNnooDu7bfjPFoTZYxMNLWUQJyrVwtbZg5gBMjTezGAJN");

            QuicConnection conn = ipfs.connect(session, peer, IPFS.CONNECT_TIMEOUT,
                    IPFS.GRACE_PERIOD, 0).get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
            assertNotNull(conn);

            conn.close();
        } finally {
            session.clear(true);
        }
    }
}
