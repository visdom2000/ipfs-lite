package threads.lite.cid;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.TypeConverter;

import com.google.protobuf.ByteString;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import threads.lite.LogUtils;


public final class Multiaddr implements Comparable<Multiaddr> {
    public static final String TAG = Multiaddr.class.getSimpleName();
    private final String address;

    private Multiaddr(String address) {
        this.address = address;
    }

    public static Multiaddr getLocalHost(int port) throws UnknownHostException {
        return Multiaddr.create(new InetSocketAddress(
                InetAddress.getLocalHost(), port));
    }

    @TypeConverter
    public static Multiaddr stringConverter(String address) {
        if (address == null) {
            return null;
        }
        return new Multiaddr(address);
    }

    @TypeConverter
    public static String multiaddressConverter(Multiaddr multiaddr) {
        if (multiaddr == null) {
            return null;
        }
        return multiaddr.toString();
    }

    @Nullable
    public static Multiaddr getSiteLocalAddress(@NonNull PeerId peerId, int port) {
        Multiaddr multiaddr = getSiteLocalAddress(port);
        if (multiaddr != null) {
            String newAddress = multiaddr + "/p2p/" + peerId.toBase58();
            return new Multiaddr(newAddress);
        }
        return null;
    }

    @Nullable
    public static Multiaddr getSiteLocalAddress(int port) {
        try {
            List<Multiaddr> list = getSiteLocalAddresses(port);
            if (!list.isEmpty()) {
                return list.get(0);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return null;
    }

    @NonNull
    public static List<Multiaddr> getSiteLocalAddresses(int port) throws SocketException {
        List<Multiaddr> multiaddrs = new ArrayList<>();
        List<NetworkInterface> interfaces = Collections.list(
                NetworkInterface.getNetworkInterfaces());
        for (NetworkInterface networkInterface : interfaces) {
            List<InetAddress> addresses =
                    Collections.list(networkInterface.getInetAddresses());
            for (InetAddress inetAddress : addresses) {
                if (inetAddress instanceof Inet4Address) {
                    if (inetAddress.isSiteLocalAddress()) {
                        multiaddrs.add(Multiaddr.create(
                                new InetSocketAddress(inetAddress, port)));
                    }
                }
            }
        }
        return multiaddrs;
    }

    private static byte[] decodeFromString(String addr) {
        while (addr.endsWith("/"))
            addr = addr.substring(0, addr.length() - 1);
        String[] parts = addr.split("/");
        if (parts[0].length() != 0)
            throw new IllegalStateException("MultiAddress must start with a /");

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            for (int i = 1; i < parts.length; ) {
                String part = parts[i++];
                Protocol p = Protocol.get(part);
                p.appendCode(outputStream);
                if (p.size() == 0)
                    continue;

                String component = parts[i++];
                if (component.length() == 0)
                    throw new IllegalStateException("Protocol requires address, but non provided!");

                outputStream.write(p.addressToBytes(component));
            }
            return outputStream.toByteArray();
        } catch (IOException e) {
            throw new IllegalStateException("Error decoding multiaddress: " + addr);
        }
    }

    public static Multiaddr create(@NonNull String address) {
        return new Multiaddr(address);
    }

    public static Multiaddr create(@NonNull ByteString raw) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();

        try (InputStream in = raw.newInput()) {
            while (in.available() > 0) {
                int code = (int) Multihash.readVarint(in);
                Protocol p = Protocol.get(code);
                stringBuilder.append("/").append(p.getType());
                if (p.size() == 0)
                    continue;

                String addr = p.readAddress(in);
                if (addr.length() > 0)
                    stringBuilder.append("/").append(addr);
            }
        }

        return new Multiaddr(stringBuilder.toString());
    }

    @NonNull
    public static Multiaddr create(@NonNull InetSocketAddress inetSocketAddress) {

        InetAddress inetAddress = inetSocketAddress.getAddress();
        boolean ipv6 = inetAddress instanceof Inet6Address;
        int port = inetSocketAddress.getPort();
        String multiaddress = "";
        if (ipv6) {
            multiaddress = multiaddress.concat("/ip6/");
        } else {
            multiaddress = multiaddress.concat("/ip4/");
        }
        multiaddress = multiaddress + inetAddress.getHostAddress() + "/udp/" + port + "/quic";
        return new Multiaddr(multiaddress);

    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean isAnyLocalAddress(InetAddress inetAddress) {
        return inetAddress.isAnyLocalAddress() || inetAddress.isLinkLocalAddress()
                || (inetAddress.isLoopbackAddress())
                || (inetAddress.isSiteLocalAddress());
    }

    @NonNull
    public InetSocketAddress getInetSocketAddress() {
        return InetSocketAddress.createUnresolved(getHost(), getPort());
    }

    private boolean isSupportedAddress() {
        try {
            InetAddress inetAddress = getInetAddress();
            return !Multiaddr.isAnyLocalAddress(inetAddress);
        } catch (Throwable ignore) {
            // nothing to do here
        }
        return false;
    }

    public boolean isSupportedProtocol(@NonNull ProtocolSupport protocolSupport) {

        if (protocolSupport == ProtocolSupport.IPv4) {
            if (isIP6()) {
                return false;
            }
        }
        if (protocolSupport == ProtocolSupport.IPv6) {
            if (isIP4()) {
                return false;
            }
        }
        if (isDnsaddr()) {
            return true;
        }
        return has(Protocol.QUIC);
    }

    public boolean isSupported(@NonNull ProtocolSupport protocolSupport) {

        if (protocolSupport == ProtocolSupport.IPv4) {
            if (isIP6()) {
                return false;
            }
        }
        if (protocolSupport == ProtocolSupport.IPv6) {
            if (isIP4()) {
                return false;
            }
        }
        if (isDnsaddr()) {
            return true;
        }
        if (isDns()) {
            return has(Protocol.QUIC);
        }
        if (isDns4()) {
            return has(Protocol.QUIC);
        }
        if (isDns6()) {
            return has(Protocol.QUIC);
        }
        if (has(Protocol.QUIC)) {
            return isSupportedAddress();
        }
        return false;
    }

    public byte[] getBytes() {
        return decodeFromString(address);
    }

    public String getHost() {
        String[] parts = toString().substring(1).split("/");
        if (parts[0].startsWith("ip") || parts[0].startsWith("dns"))
            return parts[1];
        throw new IllegalStateException("This multiaddress doesn't have a host: " + this);
    }

    public InetAddress getInetAddress() throws UnknownHostException {
        return InetAddress.getByName(getHost());
    }

    public int getPort() {
        String[] parts = toString().substring(1).split("/");
        if (parts[2].startsWith("udp"))
            return Integer.parseInt(parts[3]);
        throw new IllegalStateException("This multiaddress doesn't have a port: " + this);
    }

    @NonNull
    @Override
    public String toString() {
        return address;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Multiaddr))
            return false;
        return Objects.equals(address, ((Multiaddr) other).address);
    }

    @Override
    public int hashCode() {
        return address.hashCode();
    }

    @Nullable
    public String getStringComponent(Protocol type) {
        String[] tokens = address.split("/");
        for (int i = 0; i < tokens.length; i++) {
            String token = tokens[i];
            if (Objects.equals(token, type.getType())) {
                return tokens[i + 1];
            }
        }
        return null;
    }

    public boolean has(@NonNull Protocol type) {
        String[] tokens = address.split("/");
        for (String token : tokens) {
            if (Objects.equals(token, type.getType())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int compareTo(Multiaddr multiaddr) {
        return address.compareTo(multiaddr.address);
    }


    public boolean isIP4() {
        return address.startsWith("/ip4/");
    }

    public boolean isIP6() {
        return address.startsWith("/ip6/");
    }

    public boolean isDns() {
        return address.startsWith("/dns/");
    }

    public boolean isDns6() {
        return address.startsWith("/dns6/");
    }

    public boolean isDns4() {
        return address.startsWith("/dns4/");
    }

    public boolean isDnsaddr() {
        return address.startsWith("/dnsaddr/");
    }
}
