package threads.lite.bitswap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import net.luminis.quic.QuicConnection;

import java.io.IOException;

import bitswap.pb.MessageOuterClass;
import threads.lite.cid.Cid;
import threads.lite.core.Cancellable;
import threads.lite.format.Block;

public interface BitSwap {

    @Nullable
    Block getBlock(Cancellable cancellable, Cid cid) throws IOException;

    void clear();

    void receiveMessage(QuicConnection conn, MessageOuterClass.Message bsm);

    void throwable(@NonNull QuicConnection quicConnection, @NonNull Throwable throwable);
}
