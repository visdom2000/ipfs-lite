package threads.lite.bitswap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import net.luminis.quic.ImplementationError;
import net.luminis.quic.QuicConnection;
import net.luminis.quic.QuicStream;

import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import bitswap.pb.MessageOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Cid;
import threads.lite.core.Cancellable;
import threads.lite.format.Block;
import threads.lite.format.BlockStore;
import threads.lite.host.StreamDataHandler;
import threads.lite.host.TokenData;
import threads.lite.utils.DataHandler;


public final class BitSwapEngine implements BitSwap {
    private static final String TAG = BitSwapEngine.class.getSimpleName();
    private final BlockStore blockstore;


    public BitSwapEngine(@NonNull BlockStore bs) {
        this.blockstore = bs;
    }

    @Nullable
    @Override
    public Block getBlock(Cancellable cancellable, Cid cid) throws IOException {
        throw new ImplementationError();
    }

    @Override
    public void clear() {
        // nothing to do here
    }

    public void receiveMessage(@NonNull QuicConnection conn, @NonNull MessageOuterClass.Message bsm) {

        MessageOuterClass.Message msg = BitSwapMessage.response(blockstore, bsm);
        if (msg != null) {
            sendReply(conn, msg);
        }
    }


    @Override
    public void throwable(@NonNull QuicConnection quicConnection, @NonNull Throwable throwable) {

        LogUtils.error(TAG, "" + quicConnection.getRemoteAddress() +
                " error " + throwable.getMessage());
        if (quicConnection.isConnected()) {
            quicConnection.close();
        }
    }


    private void sendReply(@NonNull QuicConnection conn, @NonNull MessageOuterClass.Message msg) {
        try {
            conn.createStream(new StreamDataHandler(new TokenData() {
                        @Override
                        public void throwable(Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                            conn.close();
                        }

                        @Override
                        public void token(QuicStream stream, String token) throws Exception {
                            if (!Arrays.asList(IPFS.STREAM_PROTOCOL,
                                    IPFS.BITSWAP_PROTOCOL).contains(token)) {
                                throw new Exception("Token " + token + " not supported");
                            }
                            if (Objects.equals(token, IPFS.BITSWAP_PROTOCOL)) {
                                stream.writeOutput(DataHandler.encode(msg))
                                        .thenApply(QuicStream::closeOutput);
                            }
                        }

                        @Override
                        public void fin() {
                            // nothing to do here
                        }
                    }), true, IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS)
                    .thenApply(quicStream -> quicStream.writeOutput(
                            DataHandler.writeToken(IPFS.STREAM_PROTOCOL, IPFS.BITSWAP_PROTOCOL)));
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

}
