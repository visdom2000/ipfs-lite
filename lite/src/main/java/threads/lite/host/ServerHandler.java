package threads.lite.host;

import androidx.annotation.NonNull;

import net.luminis.quic.QuicConnection;
import net.luminis.quic.QuicStream;
import net.luminis.quic.server.ApplicationProtocolConnection;

import java.util.function.Consumer;

public class ServerHandler extends ApplicationProtocolConnection implements Consumer<QuicStream> {

    private final Session session;
    private final QuicConnection quicConnection;

    public ServerHandler(@NonNull Session session,
                         @NonNull QuicConnection quicConnection) {
        this.session = session;
        this.quicConnection = quicConnection;
        quicConnection.setPeerInitiatedStreamCallback(this);
    }

    @Override
    public void accept(QuicStream quicStream) {
        new StreamHandler(quicConnection, quicStream, session);
    }
}
