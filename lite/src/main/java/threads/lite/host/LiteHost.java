package threads.lite.host;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;

import net.luminis.quic.QuicClientConnection;
import net.luminis.quic.QuicClientConnectionImpl;
import net.luminis.quic.QuicConnection;
import net.luminis.quic.TransportParameters;
import net.luminis.quic.Version;
import net.luminis.quic.server.ApplicationProtocolConnection;
import net.luminis.quic.server.ApplicationProtocolConnectionFactory;
import net.luminis.quic.server.ServerConnector;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.security.KeyPair;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;

import autonat.pb.Autonat;
import circuit.pb.Circuit;
import crypto.pb.Crypto;
import identify.pb.IdentifyOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.autonat.AutoNatService;
import threads.lite.bitswap.BitSwapEngine;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.cid.Protocol;
import threads.lite.cid.ProtocolSupport;
import threads.lite.data.BlockSupplier;
import threads.lite.format.BlockStore;
import threads.lite.ident.IdentityService;
import threads.lite.push.Push;
import threads.lite.relay.RelayConnection;
import threads.lite.relay.RelayService;
import threads.lite.relay.Reservation;


public class LiteHost {


    @NonNull
    private static final String TAG = LiteHost.class.getSimpleName();

    @NonNull
    private static final AtomicInteger failure = new AtomicInteger(0);
    @NonNull
    private static final AtomicInteger success = new AtomicInteger(0);
    @NonNull
    public final AtomicReference<ProtocolSupport> protocol = new AtomicReference<>(ProtocolSupport.UNKNOWN);
    /* NOT YET REQUIRED
    @NonNull

    @NonNull
    private static final TrustManager tm = new X509TrustManager() {
        @Override
        public void checkClientTrusted(X509Certificate[] chain, String s) {
            try {
                if (IPFS.EVALUATE_PEER) {
                    for (X509Certificate cert : chain) {
                        PubKey pubKey = LiteHostCertificate.extractPublicKey(cert);
                        Objects.requireNonNull(pubKey);
                        PeerId peerId = PeerId.fromPubKey(pubKey);
                        Objects.requireNonNull(peerId);
                    }
                }
            } catch (Throwable throwable) {
                throw new RuntimeException(throwable);
            }
        }

        @Override
        public void checkServerTrusted(X509Certificate[] chain, String s) {

            try {
                if (IPFS.EVALUATE_PEER) {
                    for (X509Certificate cert : chain) {
                        PubKey pubKey = LiteHostCertificate.extractPublicKey(cert);
                        Objects.requireNonNull(pubKey);
                        PeerId peerId = PeerId.fromPubKey(pubKey);
                        Objects.requireNonNull(peerId);
                        remotes.put(peerId, pubKey);
                    }
                }
            } catch (Throwable throwable) {
                throw new RuntimeException(throwable);
            }
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
        }
    };*/
    @NonNull
    private final ConcurrentHashMap<PeerId, Reservation> reservations = new ConcurrentHashMap<>();
    @NonNull
    private final Set<InetAddress> addresses = ConcurrentHashMap.newKeySet();
    @NonNull
    private final BlockStore blockStore;
    @NonNull
    private final KeyPair keypair;
    @NonNull
    private final PeerId self;
    @NonNull
    private final LiteHostCertificate selfSignedCertificate;
    @NonNull
    private final ReentrantLock lock = new ReentrantLock();
    @NonNull
    private final ReentrantLock autonat = new ReentrantLock();
    @NonNull
    private final ReentrantLock reserve = new ReentrantLock();
    @NonNull
    private final AtomicInteger port = new AtomicInteger(0);
    @Nullable
    private Consumer<Push> incomingPush;

    @Nullable
    private ServerConnector server;

    public LiteHost(@NonNull LiteHostCertificate selfSignedCertificate,
                    @NonNull KeyPair keypair,
                    @NonNull BlockStore blockStore) throws IOException {
        this.selfSignedCertificate = selfSignedCertificate;
        this.keypair = keypair;
        this.blockStore = blockStore;
        this.self = PeerId.fromRsaPublicKey(keypair.getPublic());

        updateListenAddresses();
    }

    public static int nextFreePort() {
        int port = ThreadLocalRandom.current().nextInt(4001, 65535);
        while (true) {
            if (isLocalPortFree(port)) {
                return port;
            } else {
                port = ThreadLocalRandom.current().nextInt(4001, 65535);
            }
        }
    }

    private static boolean isLocalPortFree(int port) {
        try {
            new ServerSocket(port).close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    @NonNull
    public KeyPair getKeypair() {
        return keypair;
    }


    private void addListenAddress(@NonNull List<InetAddress> inetAddresses) {
        addresses.addAll(inetAddresses);
    }

    public void start(int port, Consumer<QuicConnection> connectConsumer,
                      Consumer<QuicConnection> closedConsumer,
                      Consumer<Reachability> reachabilityConsumer) {
        if (port >= 0 && !isLocalPortFree(port)) {
            this.port.set(nextFreePort());
        } else {
            this.port.set(port);
        }
        if (this.port.get() > 0) {
            try {
                List<Version> supportedVersions = new ArrayList<>();
                supportedVersions.add(Version.IETF_draft_29);
                supportedVersions.add(Version.QUIC_version_1);
                Session serverSession = new Session(new BitSwapEngine(blockStore), this);
                server = new ServerConnector(port, closedConsumer, reachabilityConsumer,
                        new FileInputStream(selfSignedCertificate.certificate()),
                        new FileInputStream(selfSignedCertificate.privateKey()),
                        supportedVersions, false);
                server.registerApplicationProtocol(IPFS.ALPN, new ApplicationProtocolConnectionFactory() {
                    @Override
                    public ApplicationProtocolConnection createConnection(
                            String protocol, QuicConnection quicConnection) {

                        try {
                            connectConsumer.accept(quicConnection);
                        } catch (Throwable ignore) {
                        }

                        try {
                            InetSocketAddress address = quicConnection.getRemoteAddress();

                            if (!Multiaddr.isAnyLocalAddress(address.getAddress())) {
                                reachabilityConsumer.accept(Reachability.GLOBAL);
                            }
                        } catch (Throwable ignore) {
                        }

                        LogUtils.error(TAG, "Server connection established " +
                                quicConnection.getRemoteAddress().toString());


                        return new ServerHandler(serverSession, quicConnection);

                    }
                });
                server.start();
            } catch (Throwable throwable) {
                reachabilityConsumer.accept(Reachability.NONE);
                LogUtils.error(TAG, throwable);
            }
        } else {
            reachabilityConsumer.accept(Reachability.NONE);
        }
    }

    @NonNull
    public LiteHostCertificate getSelfSignedCertificate() {
        return selfSignedCertificate;
    }

    @NonNull
    public ConcurrentHashMap<PeerId, Reservation> reservations() {
        return reservations;
    }

    @NonNull
    public Session createSession() {
        return createSession(blockSupplier -> {
        }, false);
    }

    @NonNull
    public Session createSession(@NonNull Consumer<BlockSupplier> supplier,
                                 boolean findProvidersActive) {
        return new Session(blockStore, this, supplier, findProvidersActive);
    }

    public PeerId self() {
        return self;
    }


    @NonNull
    private List<Multiaddr> prepareAddresses(@NonNull Set<Multiaddr> multiaddrs) {
        List<Multiaddr> all = new ArrayList<>();
        for (Multiaddr multiaddr : multiaddrs) {

            if (multiaddr.isDns()) {
                Multiaddr ma = DnsResolver.resolveDns(protocol.get(), multiaddr);
                if (ma != null) {
                    all.add(ma);
                }
            } else if (multiaddr.isDns6()) {
                Multiaddr ma = DnsResolver.resolveDns6(protocol.get(), multiaddr);
                if (ma != null) {
                    all.add(ma);
                }
            } else if (multiaddr.isDns4()) {
                Multiaddr ma = DnsResolver.resolveDns4(protocol.get(), multiaddr);
                if (ma != null) {
                    all.add(ma);
                }
            } else if (multiaddr.isDnsaddr()) {
                all.addAll(DnsResolver.resolveDnsAddress(protocol.get(), multiaddr));
            } else {
                all.add(multiaddr);
            }

        }
        return supported(all);
    }

    @NonNull
    public List<Multiaddr> supported(@NonNull List<Multiaddr> all) {
        List<Multiaddr> result = new ArrayList<>();
        for (Multiaddr ma : all) {
            if (ma.isSupportedProtocol(protocol.get())) {
                result.add(ma);
            }
        }
        return result;
    }


    @NonNull
    public List<Multiaddr> listenAddresses() {
        try {
            List<Multiaddr> list = new ArrayList<>();
            if (port.get() > 0) {
                for (InetAddress inetAddress : addresses) {
                    Multiaddr multiaddr = Multiaddr.create(
                            new InetSocketAddress(inetAddress, port.get()));
                    list.add(multiaddr);
                }
            }
            return list;
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return Collections.emptyList();

    }

    @NonNull
    public CompletableFuture<QuicConnection> connectPeer(
            @NonNull Session session, @NonNull PeerId peerId, int timeout,
            int initialMaxStreams, int initialMaxStreamData, boolean acceptLocal) {

        CompletableFuture<QuicConnection> found = new CompletableFuture<>();

        session.findPeer(() -> found.isDone() || found.isCancelled(), multiaddr -> {
            LogUtils.error(TAG, multiaddr.toString());
            if (!found.isDone()) {
                try {
                    found.complete(connect(session, Set.of(multiaddr),
                            timeout, IPFS.GRACE_PERIOD,
                            initialMaxStreams, initialMaxStreamData).
                            get(timeout, TimeUnit.SECONDS));
                } catch (ExecutionException | TimeoutException | InterruptedException ignore) {
                    // nothing to do here (maybe another search has valid peer addresses)
                }
            }
        }, peerId, acceptLocal);

        return found;

    }

    @NonNull
    public CompletableFuture<QuicConnection> connect(
            @NonNull Session session, @NonNull Set<Multiaddr> multiaddrs, int timeout,
            int maxIdleTimeoutInSeconds, int initialMaxStreams, int initialMaxStreamData) {

        CompletableFuture<QuicConnection> done = new CompletableFuture<>();
        List<Multiaddr> multiaddr = prepareAddresses(multiaddrs);
        int addresses = multiaddr.size();
        if (addresses == 0) {
            done.completeExceptionally(new ConnectException("no addresses left"));
            return done;
        }

        ExecutorService executor = Executors.newFixedThreadPool(
                Runtime.getRuntime().availableProcessors());
        AtomicInteger counter = new AtomicInteger(0);
        for (Multiaddr address : multiaddr) {
            counter.incrementAndGet();
            executor.execute(() -> {
                try {
                    QuicConnection conn = dial(session, address, timeout,
                            maxIdleTimeoutInSeconds, initialMaxStreams,
                            initialMaxStreamData);
                    done.complete(conn);
                    executor.shutdownNow();
                } catch (Throwable throwable) {
                    if (counter.decrementAndGet() == 0) {
                        if (!done.isDone()) {
                            done.completeExceptionally(throwable);
                        }
                    }
                }
            });

        }
        executor.shutdown();
        return done;


    }

    // todo when sdk version min is sdk31, then return CompletableFuture<QuicConnection>
    @NonNull
    public QuicClientConnection dial(@NonNull Session session, @NonNull Multiaddr address,
                                     int timeout, int maxIdleTimeoutInSeconds,
                                     int initialMaxStreams, int initialMaxStreamData)
            throws ConnectException, CancellationException {


        LiteHostCertificate selfSignedCertificate = getSelfSignedCertificate();

        long start = System.currentTimeMillis();
        boolean run = false;

        int initialMaxData = initialMaxStreamData;
        if (initialMaxStreams > 0) {
            initialMaxData = Integer.MAX_VALUE;
        }

        QuicClientConnectionImpl.Builder builder = QuicClientConnectionImpl.newBuilder()
                .version(Version.IETF_draft_29) // in the future switch to version 1
                .noServerCertificateCheck()
                .clientCertificate(selfSignedCertificate.cert())
                .clientCertificateKey(selfSignedCertificate.key())
                .host(address.getHost())
                .port(address.getPort())
                .alpn(IPFS.ALPN)
                .transportParams(new TransportParameters(
                        maxIdleTimeoutInSeconds, initialMaxData, initialMaxStreamData,
                        initialMaxStreams, 0));

        QuicClientConnection conn = builder.build();

        try {
            conn.connect(timeout).get(timeout, TimeUnit.SECONDS); // todo future remove get


            if (initialMaxStreams > 0) {
                conn.setPeerInitiatedStreamCallback(
                        stream -> new StreamHandler(conn, stream, session));
            }

            // LogUtils.error(TAG, conn.getNewSessionTickets().toString());

            run = true;
            return conn;
        } catch (ExecutionException | TimeoutException exception) {
            conn.abortHandshake(); // todo hack sdk31
            throw new ConnectException(exception.getMessage());
        } catch (InterruptedException interruptedException) {
            conn.abortHandshake(); // todo hack sdk31
            throw new CancellationException(interruptedException.getMessage());
        } finally {
            if (LogUtils.isError()) {
                if (run) {
                    success.incrementAndGet();
                } else {
                    failure.incrementAndGet();
                }
                LogUtils.debug(TAG, "Run dialClient " + run +
                        " Success " + success.get() +
                        " Failure " + failure.get() +
                        " Address " + address +
                        " Time " + (System.currentTimeMillis() - start));
            }
        }
    }

    public boolean hasReservation(@NonNull PeerId relayId) {
        return reservations.containsKey(relayId);
    }

    public void push(@NonNull QuicConnection connection, @NonNull byte[] data) {
        try {
            Objects.requireNonNull(connection);
            Objects.requireNonNull(data);
            if (incomingPush != null) {
                incomingPush.accept(new Push(connection, new String(data)));
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    public void setIncomingPush(@Nullable Consumer<Push> incomingPush) {
        this.incomingPush = incomingPush;
    }

    public int numServerConnections() {
        if (server != null) {
            return server.numConnections();
        }
        return 0;
    }


    public IdentifyOuterClass.Identify createIdentity(@Nullable InetSocketAddress inetSocketAddress) {

        IdentifyOuterClass.Identify.Builder builder = IdentifyOuterClass.Identify.newBuilder()
                .setAgentVersion(IPFS.AGENT)
                .setPublicKey(ByteString.copyFrom(
                        Crypto.PublicKey.newBuilder().setType(Crypto.KeyType.RSA).
                                setData(ByteString.copyFrom(
                                        keypair.getPublic().getEncoded())).build().toByteArray()))
                .setProtocolVersion(IPFS.PROTOCOL_VERSION);

        List<Multiaddr> addresses = listenAddresses();
        if (!addresses.isEmpty()) {
            for (Multiaddr addr : addresses) {
                builder.addListenAddrs(ByteString.copyFrom(addr.getBytes()));
            }
        }
        List<String> protocols = getProtocols();
        for (String protocol : protocols) {
            builder.addProtocols(protocol);
        }

        if (inetSocketAddress != null) {
            Multiaddr observed = Multiaddr.create(inetSocketAddress);
            builder.setObservedAddr(ByteString.copyFrom(observed.getBytes()));
        }

        return builder.build();
    }

    private List<String> getProtocols() {
        return Arrays.asList(IPFS.STREAM_PROTOCOL, IPFS.PUSH_PROTOCOL, IPFS.BITSWAP_PROTOCOL,
                IPFS.IDENTITY_PROTOCOL, IPFS.DHT_PROTOCOL, IPFS.RELAY_PROTOCOL_STOP);
    }

    public void shutdown() {
        try {
            if (server != null) {
                server.shutdown();
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            server = null;
            port.set(0);
        }
    }

    public void updateNetwork() {
        this.addresses.clear();
        updateListenAddresses();
    }

    public void updateListenAddresses() {
        lock.lock();
        try {
            List<InetAddress> externals = new ArrayList<>();
            List<NetworkInterface> interfaces = Collections.list(
                    NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface networkInterface : interfaces) {

                List<InetAddress> addresses =
                        Collections.list(networkInterface.getInetAddresses());
                for (InetAddress inetAddress : addresses) {

                    if (!Multiaddr.isAnyLocalAddress(inetAddress)) {
                        externals.add(inetAddress);
                    }
                }

            }

            if (!externals.isEmpty()) {
                protocol.set(getProtocol(externals));
                addListenAddress(externals);
            } else {
                protocol.set(ProtocolSupport.IPv4);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            lock.unlock();
        }
    }

    @NonNull
    private ProtocolSupport getProtocol(@NonNull List<InetAddress> addresses) {
        boolean ipv4 = false;
        boolean ipv6 = false;
        for (InetAddress inet : addresses) {
            if (inet instanceof Inet6Address) {
                ipv6 = true;
            } else {
                ipv4 = true;
            }
        }

        if (ipv4 && ipv6) {
            return ProtocolSupport.UNKNOWN;
        } else if (ipv4) {
            return ProtocolSupport.IPv4;
        } else if (ipv6) {
            return ProtocolSupport.IPv6;
        } else {
            return ProtocolSupport.UNKNOWN;
        }

    }

    public int getPort() {
        return port.get();
    }

    public void autonat(@NonNull Session session, long timeout) {
        autonat.lock();
        try {
            Set<Multiaddr> bootstrap = getBootstrap();
            if (!bootstrap.isEmpty()) {
                ExecutorService executor = Executors.newFixedThreadPool(
                        Runtime.getRuntime().availableProcessors());
                for (Multiaddr address : bootstrap) {

                    executor.execute(() -> {
                        try {
                            QuicClientConnection conn = dial(session, address,
                                    IPFS.CONNECT_TIMEOUT, IPFS.GRACE_PERIOD_RESERVATION,
                                    IPFS.MAX_STREAMS, IPFS.MESSAGE_SIZE_MAX);
                            Objects.requireNonNull(conn);

                            PeerInfo peerInfo = IdentityService.getPeerInfo(conn).
                                    get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);

                            Multiaddr observed = peerInfo.getObserved();
                            if (observed != null) {
                                try {
                                    InetAddress inetAddress = observed.getInetAddress();
                                    addListenAddress(List.of(inetAddress));
                                } catch (UnknownHostException ignore) {
                                }
                            }

                            if (peerInfo.hasProtocol(IPFS.AUTONAT_PROTOCOL)) {
                                try {
                                    CompletableFuture<Autonat.Message> res =
                                            AutoNatService.getDialResponse(conn, self, listenAddresses());
                                    Autonat.Message response = res.get(timeout, TimeUnit.SECONDS);
                                    LogUtils.error(TAG, response.getDialResponse().getStatusText());
                                } catch (Throwable throwable) {
                                    LogUtils.error(TAG, throwable);
                                }
                            }
                            conn.close();
                        } catch (Throwable ignore) {
                        }
                    });
                }
                executor.shutdown();
                if (timeout > 0) {
                    try {
                        boolean termination = executor.awaitTermination(timeout, TimeUnit.SECONDS);
                        if (!termination) {
                            executor.shutdownNow();
                        }
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }
            }

        } finally {
            autonat.unlock();
        }
    }


    public long reservations(@NonNull Session session, long timeout) {
        reserve.lock();
        try {
            AtomicLong minInMinutes = new AtomicLong(60);

            ConcurrentHashMap<PeerId, Reservation> relays = reservations();
            // check if reservations are still valid and not expired
            for (PeerId relayId : relays.keySet()) {

                Reservation reservation = relays.get(relayId);
                Objects.requireNonNull(reservation);

                // check if still a connection
                // only for safety here
                if (!reservation.getConnection().isConnected()) {
                    relays.remove(relayId);
                    continue;
                }
                try {

                    if (reservation.expireInMinutes() < 2) {
                        try {
                            Multiaddr multiaddr = reservation.getMultiaddr();
                            reservation = reservation(session, relayId, multiaddr);
                            minInMinutes.set(Math.min(minInMinutes.get(),
                                    reservation.expireInMinutes()));
                        } catch (Throwable throwable) {
                            relays.remove(relayId);
                        }
                    } else {
                        minInMinutes.set(Math.min(minInMinutes.get(),
                                reservation.expireInMinutes()));
                    }
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }

            Set<Multiaddr> bootstrap = getBootstrap();
            if (!bootstrap.isEmpty()) {
                ExecutorService executor = Executors.newFixedThreadPool(
                        Runtime.getRuntime().availableProcessors());
                for (Multiaddr address : bootstrap) {
                    try {
                        String name = address.getStringComponent(Protocol.P2P);
                        Objects.requireNonNull(name);
                        PeerId relayId = PeerId.fromBase58(name);
                        Objects.requireNonNull(relayId);

                        if (hasReservation(relayId)) {
                            // nothing to do here, reservation is sill valid
                            continue;
                        }

                        executor.execute(() -> {
                            try {
                                Reservation reservation = reservation(session,
                                        relayId, address);
                                minInMinutes.set(Math.min(minInMinutes.get(),
                                        reservation.expireInMinutes()));
                            } catch (Throwable ignore) {
                            }
                        });
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }
                executor.shutdown();
                if (timeout > 0) {
                    try {
                        boolean termination = executor.awaitTermination(timeout, TimeUnit.SECONDS);
                        if (!termination) {
                            executor.shutdownNow();
                        }
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }
            }
            return minInMinutes.get();
        } finally {
            reserve.unlock();
        }
    }

    @NonNull
    private Reservation reservation(@NonNull Session session, @NonNull PeerId relayId,
                                    @NonNull Multiaddr multiaddr) throws Exception {

        if (!multiaddr.isSupported(protocol.get())) {
            throw new Exception("address is not supported");
        }

        QuicClientConnection conn = dial(session, multiaddr,
                IPFS.CONNECT_TIMEOUT, IPFS.GRACE_PERIOD_RESERVATION,
                IPFS.MAX_STREAMS, IPFS.MESSAGE_SIZE_MAX);
        Objects.requireNonNull(conn);

        // check if RELAY protocols HOP is supported
        PeerInfo peerInfo = IdentityService.getPeerInfo(conn).
                get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);

        LogUtils.error(TAG, peerInfo.getProtocols().toString());

        if (!peerInfo.hasProtocol(IPFS.RELAY_PROTOCOL_HOP)) {
            conn.close();
            throw new Exception("does not support relay hop");
        }


        Circuit.Reservation reservation = RelayService.reserve(conn);
        Reservation done = new Reservation(relayId, conn, multiaddr, reservation);
        reservations.put(relayId, done);
        return done;
    }


    public RelayConnection createRelayConnection(@NonNull Session session, @NonNull PeerId peerId,
                                                 @NonNull Multiaddr relay) throws Exception {
        return RelayService.createRelayConnection(this, session, peerId, relay,
                IPFS.CONNECT_TIMEOUT, IPFS.GRACE_PERIOD_RESERVATION, IPFS.MAX_STREAMS,
                IPFS.MESSAGE_SIZE_MAX);
    }

    public boolean hasReservations() {
        return reservations.size() > 0;
    }

    public boolean isServerRunning() {
        return server != null;
    }

    public Set<Multiaddr> getBootstrap() {
        return DnsResolver.resolveDnsAddress(protocol.get(), IPFS.LIB2P_DNS);
    }
}


