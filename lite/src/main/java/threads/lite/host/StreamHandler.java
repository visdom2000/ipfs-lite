package threads.lite.host;


import androidx.annotation.NonNull;

import net.luminis.quic.QuicConnection;
import net.luminis.quic.QuicStream;

import java.nio.ByteBuffer;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

import bitswap.pb.MessageOuterClass;
import circuit.pb.Circuit;
import identify.pb.IdentifyOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.utils.DataHandler;


public class StreamHandler {
    private static final String TAG = StreamHandler.class.getSimpleName();
    private final AtomicReference<String> tokenProtocol =
            new AtomicReference<>(null);

    public StreamHandler(@NonNull QuicConnection quicConnection,
                         @NonNull QuicStream stream,
                         @NonNull Session session) {
        stream.setStreamDataConsumer(new StreamDataHandler(new StreamData() {

            @Override
            public void throwable(Throwable throwable) {
                session.throwable(quicConnection, throwable);
            }

            @Override
            public void token(QuicStream stream, String token) {

                tokenProtocol.set(token);
                switch (token) {
                    /* NOT YET SUPPORTED
                    case IPFS.RELAY_PROTOCOL_HOP:
                        outputStream.write(DataHandler.writeToken(IPFS.RELAY_PROTOCOL_HOP));
                        break;
                    */
                    /* NOT YET SUPPORTED
                    case IPFS.PING_PROTOCOL:
                        outputStream.write(DataHandler.writeToken(IPFS.PING_PROTOCOL));
                        outputStream.close();
                        break;*/
                    case IPFS.RELAY_PROTOCOL_STOP:
                        stream.writeOutput(DataHandler.writeToken(IPFS.RELAY_PROTOCOL_STOP));
                        break;
                    case IPFS.STREAM_PROTOCOL:
                        stream.writeOutput(DataHandler.writeToken(IPFS.STREAM_PROTOCOL));
                        break;
                    case IPFS.PUSH_PROTOCOL:
                        stream.writeOutput(DataHandler.writeToken(IPFS.PUSH_PROTOCOL))
                                .thenApply(QuicStream::closeOutput);
                        break;
                    case IPFS.BITSWAP_PROTOCOL:
                        stream.writeOutput(DataHandler.writeToken(IPFS.BITSWAP_PROTOCOL))
                                .thenApply(QuicStream::closeOutput);
                        break;
                    case IPFS.IDENTITY_PROTOCOL:
                        stream.writeOutput(DataHandler.writeToken(IPFS.IDENTITY_PROTOCOL));

                        IdentifyOuterClass.Identify response =
                                session.getHost().createIdentity(
                                        quicConnection.getRemoteAddress());
                        stream.writeOutput(DataHandler.encode(response))
                                .thenApply(QuicStream::closeOutput);
                        break;
                    case IPFS.NA:
                        stream.closeOutput();
                        break;
                    default:
                        LogUtils.debug(TAG, "Ignore " + token +
                                " StreamId " + stream.getStreamId());
                        stream.writeOutput(DataHandler.writeToken(IPFS.NA))
                                .thenApply(QuicStream::closeOutput);
                }

            }

            @Override
            public void fin() {
                // just received fin on input stream
            }

            @Override
            public void data(QuicStream stream, ByteBuffer data) throws Exception {
                String protocol = tokenProtocol.get();
                if (protocol != null) {
                    switch (protocol) {
                        case IPFS.BITSWAP_PROTOCOL: {
                            session.receiveMessage(quicConnection,
                                    MessageOuterClass.Message.parseFrom(data.array()));
                            break;
                        }
                        case IPFS.PUSH_PROTOCOL: {
                            session.getHost().push(quicConnection, data.array());
                            break;
                        }
                        /* NOT YET SUPPORTED
                        case IPFS.RELAY_PROTOCOL_HOP: {
                                    Circuit.HopMessage hopMessage = Circuit.HopMessage.parseFrom(message);
                                    Objects.requireNonNull(hopMessage);

                                    outputStream.write(DataHandler.encode(Circuit.HopMessage.newBuilder()
                                            .setType(Circuit.HopMessage.Type.STATUS)
                                            .setStatus(Circuit.Status.PERMISSION_DENIED)
                                            .build()));

                                    break;
                        }*/
                        case IPFS.RELAY_PROTOCOL_STOP: {
                            LogUtils.debug(TAG, "RELAY_PROTOCOL_STOP");
                            Circuit.StopMessage stopMessage = Circuit.StopMessage.parseFrom(
                                    data.array());
                            Objects.requireNonNull(stopMessage);

                            if (stopMessage.hasPeer()) {
                                Circuit.StopMessage.Builder builder =
                                        Circuit.StopMessage.newBuilder()
                                                .setType(Circuit.StopMessage.Type.STATUS);
                                builder.setStatus(Circuit.Status.OK);
                                stream.writeOutput(DataHandler.encode(builder.build()));
                            }
                            break;
                        }
                        default:
                            throw new Exception("StreamHandler invalid protocol");
                    }
                } else {
                    throw new Exception("StreamHandler invalid protocol");
                }
            }
        }));
    }
}
